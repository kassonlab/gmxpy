==========
Change Log
==========

API and Python module versioning information.

List of features and available or target versions, organized by component.
(See :doc:`components`). Link to documentation in :doc:`reference` section and
appropriate content in :doc:`userguide` or :doc:`developerguide` if available.
Should be accompanied by appropriate annotations elsewhere in the docs.
I.e. See `Sphinx docs. <http://www.sphinx-doc.org/en/stable/markup/para.html>`_

.. rubric:: 0.0.1

Runner supports simulation from TPR file.

.. rubric:: 0.0.0

Design docs and architecture requirements.

Roadmap
=======

.. rubric:: 0.0.2

Runner supports updating target simulation steps and submitting additional runs in a single program context.

Some introspection of simulation state exposed.

Abstraction of Integrator object to allow interaction outside of calls to ``run()``
